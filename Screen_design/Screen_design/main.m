//
//  main.m
//  Screen_design
//
//  Created by Clicklabs 104 on 10/8/15.
//  Copyright (c) 2015 cl. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
