//
//  ViewController.m
//  Screen_design
//
//  Created by Clicklabs 104 on 10/8/15.
//  Copyright (c) 2015 cl. All rights reserved.
//

#import "ViewController.h"


@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *editprofile;
@property (weak, nonatomic) IBOutlet UIButton *arrow;

@property (weak, nonatomic) IBOutlet UIButton *done;
@property (weak, nonatomic) IBOutlet UIImageView *profilepic;
@property (weak, nonatomic) IBOutlet UIImageView *background;
@property (weak, nonatomic) IBOutlet UILabel *infolabel;
@property (weak, nonatomic) IBOutlet UITextField *firstname;

@property (weak, nonatomic) IBOutlet UITextField *lastname;
@property (weak, nonatomic) IBOutlet UITextField *email;
@property (weak, nonatomic) IBOutlet UITextField *phone;
@property (weak, nonatomic) IBOutlet UIScrollView *scroll;
@property (weak, nonatomic) IBOutlet UIButton *changepassword;
@property (weak, nonatomic) IBOutlet UIButton *change;
@end

@implementation ViewController
@synthesize firstname;
@synthesize lastname;
@synthesize email;
@synthesize phone;
@synthesize done;
@synthesize changepassword;
@synthesize profilepic;
@synthesize background;
@synthesize arrow;
@synthesize editprofile;
@synthesize scroll;
@synthesize change;
- (IBAction)change:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"you can change your password" message:@"" delegate:(nil) cancelButtonTitle: @"OK" otherButtonTitles: nil];
    [alert show];

}

- (IBAction)done:(id)sender {
    
    if ([firstname.text length] == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"All fields are mandatory" message:@"Enter your first name" delegate:(nil) cancelButtonTitle: @"OK" otherButtonTitles: nil];
        [alert show];
    }
    else if ([lastname.text length] == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"All fields are mandatory" message:@"Enter your last name" delegate:(nil) cancelButtonTitle: @"OK" otherButtonTitles:nil];
        [alert show];
    }
    else if ([email.text length] == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"All fields are mandatory" message:@"Enter your email" delegate:(nil) cancelButtonTitle: @"OK" otherButtonTitles:nil];
        [alert show];
    }

    else if ([phone.text length] == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"All fields are mandatory" message:@"Enter your phone number" delegate:(nil) cancelButtonTitle: @"OK" otherButtonTitles:nil];
        [alert show];
    }
    else if ([phone.text length] < 10)
    {
        UIAlertController *alertController = [UIAlertController  alertControllerWithTitle:@"Phone number must be of 10 digits "  message:nil preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }]];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    
    else if ([phone.text length] > 10)
    {
        UIAlertController *alertController = [UIAlertController  alertControllerWithTitle:@"Phone number must be of 10 digits only"  message:nil preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }]];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else if ([self name: firstname.text ] == YES)
    {
        //firstname.textColor= [UIColor redColor];
        UIAlertController *alertController = [UIAlertController  alertControllerWithTitle:@"You have entered digits in first name"  message:@" Enter characters "  preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }]];
        [self presentViewController:alertController animated:YES completion:nil];
        
    }
    
    else if ([self name: lastname.text ] == YES)
    {
       // lastname.textColor= [UIColor redColor];
        UIAlertController *alertController = [UIAlertController  alertControllerWithTitle:@"You have entered digits in last name"  message:@"Enter characters "  preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }]];
        [self presentViewController:alertController animated:YES completion:nil];
        
    }
    else if ([self number: phone.text]== YES)
    {
        //phone.textColor= [UIColor redColor];
        UIAlertController *alertController = [UIAlertController  alertControllerWithTitle:@"You have entered characters"  message:@"Enter digits "  preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }]];
        [self presentViewController:alertController animated:YES completion:nil];
    }

    else if (![email.text isEqualToString:@""]) {
        
        NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
        if ([emailTest evaluateWithObject:email.text] == YES)
        { }
        else{
            UIAlertController *alertController = [UIAlertController  alertControllerWithTitle:@"Invalid email"  message:@"Enter your valid email id"  preferredStyle:UIAlertControllerStyleAlert];
            [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                [self dismissViewControllerAnimated:YES completion:nil];
            }]];
            [self presentViewController:alertController animated:YES completion:nil];

        }
        UIAlertController *alertController = [UIAlertController  alertControllerWithTitle:@"Done"  message:@"Profile edited"  preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }]];
        [self presentViewController:alertController animated:YES completion:nil];

    }
}

-(BOOL)name :(NSString*) text{
    NSCharacterSet *set =[NSCharacterSet characterSetWithCharactersInString:@"1234567890"];
    if([text rangeOfCharacterFromSet:set].location== NSNotFound )
    {
        return NO;
    }
    else
    {
        return YES;
    }
}
-(BOOL) number:  (NSString*) text{
    NSCharacterSet *set =[[NSCharacterSet characterSetWithCharactersInString:@"1234567890"]invertedSet];
    if([text rangeOfCharacterFromSet:set].location== NSNotFound )
    {
        return NO;
    }
    else
    {
        return YES;
    }
}



-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    profilepic.layer.cornerRadius= 105/2;
    profilepic.layer.masksToBounds=YES;
    [scroll setScrollEnabled:YES];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismiss:)];
    
    [self.view addGestureRecognizer:tap];

    // Do any additional setup after loading the view, typically from a nib.
}
-(void) dismiss:sender{
    [firstname resignFirstResponder];
    [lastname resignFirstResponder];
    [email resignFirstResponder];
    [phone resignFirstResponder];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
